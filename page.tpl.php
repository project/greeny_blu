<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <title><?php print $head_title ?></title>
 <link href="style.css" rel="stylesheet" type="text/css" />
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="shadow_left">&nbsp;</td>
    <td class="header_column">
	<table width="100%" border="0" cellspacing="10" cellpadding="0">
      <tr>
        <td class="logo_area">    
      <?php if ($site_name) { ?><h1><a style="text-decoration:none" href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
        </td>
        <td width="300">
		  	Search the website <br /><?php print $search_box ?>
          </td>
      </tr>
    </table></td>
    <td class="shadow_right">&nbsp;</td>
  </tr>
  <tr>
    <td class="horizontal_column">&nbsp;</td>
    <td class="horizontal_center"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="linkcontainer">
      <tr>

<?php if (is_array($primary_links)) : ?>
<tr>
<?php foreach ($primary_links as $link): ?>
<td><div class="navigation"><?php

print "<a href='" . $link['href'] . "' class=\"main_link\">" . $link['title'] . "</a>";

?></div></td>

<?php endforeach; ?>
</tr>
<?php endif; ?>
</div>

      </tr>
    </table></td>
    <td class="horizontal_column">&nbsp;</td>
  </tr>
  <tr>
       <?php if ($sidebar_right) { ?>    <td class="shadow_left">&nbsp;</td>
    <td class="below_header">
       <?php print $sidebar_right ?></td>
    <td class="shadow_right">&nbsp;</td>
         </tr><?php } ?>          
	    </div>

  <tr>
    <td class="shadow_left">&nbsp;</td>
    <td class="main_content_box"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
       <?php if ($sidebar_left) { ?><td class="left_content">
       <?php print $sidebar_left ?>
       </td><?php } ?>
        <td class="body_content"><h2><?php print $title ?></h2> <br />
          <br />
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>
        </td>
      </tr>
    </table></td>
    <td class="shadow_right">&nbsp;</td>
  </tr>
  <tr>
    <td class="shadow_left">&nbsp;</td>
    <td class="middle_spacer"><div class="bottom_content"><?php print $footer_message ?></div></td>
    <td class="shadow_right">&nbsp;</td>
  </tr>
  <tr>
    <td class="shadow_left">&nbsp;</td>	  
    <!-- It is greatly appreciated that you leave the below theme information here -->
    <td class="bottom_link_container">Theme created by <a href="http://www.ronsnexus.com/">Ron Williams</a> for <a href="http://www.lithicmedia.com/">Lithic Media</a></td>
    <td class="shadow_right">&nbsp;</td>
  </tr>
</table>
</body>
</html>
